package fun.huixi.weiju.mapper;

import fun.huixi.weiju.pojo.entity.WjDynamicComment;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 动态评论表 Mapper 接口
 * </p>
 *
 * @author 叶秋
 * @since 2021-11-02
 */
public interface WjDynamicCommentMapper extends BaseMapper<WjDynamicComment> {

}
