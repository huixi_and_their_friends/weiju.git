package fun.huixi.weiju.mapper;

import fun.huixi.weiju.pojo.entity.WjAppealComment;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 诉求-评论 Mapper 接口
 * </p>
 *
 * @author 叶秋
 * @since 2021-11-02
 */
public interface WjAppealCommentMapper extends BaseMapper<WjAppealComment> {

}
