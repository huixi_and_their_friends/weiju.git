package fun.huixi.weiju.service.impl;

import cn.hutool.core.bean.BeanUtil;
import fun.huixi.weiju.exception.BusinessException;
import fun.huixi.weiju.pojo.dto.appeal.SaveAppealCommentDTO;
import fun.huixi.weiju.pojo.entity.WjAppealComment;
import fun.huixi.weiju.mapper.WjAppealCommentMapper;
import fun.huixi.weiju.service.WjAppealCommentService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 诉求-评论 服务实现类
 * </p>
 *
 * @author 叶秋
 * @since 2021-11-02
 */
@Service
public class WjAppealCommentServiceImpl extends ServiceImpl<WjAppealCommentMapper, WjAppealComment> implements WjAppealCommentService {

    /**
     * 保存诉求评论
     *
     * @param userId               用户id
     * @param saveAppealCommentDTO 保存参数
     * @return void
     * @Author 叶秋
     * @Date 2021/11/16 11:39
     **/
    @Override
    public void saveAppealComment(Integer userId, SaveAppealCommentDTO saveAppealCommentDTO) {

        WjAppealComment wjAppealComment = new WjAppealComment();
        BeanUtil.copyProperties(saveAppealCommentDTO, wjAppealComment);

        wjAppealComment.setUserId(userId);

        boolean save = this.save(wjAppealComment);

        if(!save){
            throw new BusinessException("保存诉求评论失败");
        }


    }
}
