package fun.huixi.weiju.service;

import fun.huixi.weiju.pojo.entity.WjAppealAddress;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 存储诉求的地址信息 服务类
 * </p>
 *
 * @author 叶秋
 * @since 2021-11-02
 */
public interface WjAppealAddressService extends IService<WjAppealAddress> {

}
