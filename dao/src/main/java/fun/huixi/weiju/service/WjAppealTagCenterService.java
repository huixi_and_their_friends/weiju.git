package fun.huixi.weiju.service;


import com.baomidou.mybatisplus.extension.service.IService;
import fun.huixi.weiju.pojo.entity.WjAppealTagCenter;

/**
 * 诉求与标志的对应关系 服务类
 *
 * @author 叶秋
 * @since 2021-11-10
 */
public interface WjAppealTagCenterService extends IService<WjAppealTagCenter> {

}
