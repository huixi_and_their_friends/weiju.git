package fun.huixi.weiju.service;

import fun.huixi.weiju.pojo.entity.WjDynamicEndorse;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 动态的点赞表 服务类
 * </p>
 *
 * @author 叶秋
 * @since 2021-11-02
 */
public interface WjDynamicEndorseService extends IService<WjDynamicEndorse> {

    /**
     * 查询用户是否点赞动态
     *
     * @param userId        用户ID
     * @param dynamicIdList 动态ID
     * @return
     */
    Map<Integer, Boolean> queryUserOrEndorseByUserId(Integer userId, List<Integer> dynamicIdList);


    /**
     *  判断用户是否点赞
     * @Author 叶秋
     * @Date 2021/11/24 11:18
     * @param userId 用户id
     * @param dynamicId 动态id
     * @return java.lang.Boolean
     **/
    Boolean judgeUserOrEndorse(Integer userId, Integer dynamicId);
}
