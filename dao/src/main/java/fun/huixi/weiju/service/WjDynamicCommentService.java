package fun.huixi.weiju.service;

import fun.huixi.weiju.pojo.entity.WjDynamicComment;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 动态评论表 服务类
 * </p>
 *
 * @author 叶秋
 * @since 2021-11-02
 */
public interface WjDynamicCommentService extends IService<WjDynamicComment> {


    /**
     *  判断这条评论是否是这位用户的
     * @Author 叶秋
     * @Date 2021/11/29 14:52
     * @param userId  用户id
     * @param dynamicCommentId  动态评论id
     * @return java.lang.Boolean
     **/
    Boolean judgeCommentOrMe(Integer userId, Integer dynamicCommentId);
}
