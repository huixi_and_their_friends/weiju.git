package fun.huixi.weiju.business;

import fun.huixi.weiju.page.PageData;
import fun.huixi.weiju.pojo.dto.appeal.PageQueryAppealCommentDTO;
import fun.huixi.weiju.pojo.dto.appeal.SaveAppealCommentDTO;
import fun.huixi.weiju.pojo.entity.WjAppealTag;
import fun.huixi.weiju.pojo.vo.appeal.AppealCommentItemVO;
import fun.huixi.weiju.pojo.vo.appeal.AppealItemVO;
import fun.huixi.weiju.pojo.dto.appeal.PageQueryAppealDTO;

import java.util.List;
import java.util.Map;


/**
 *  帖子相关的业务方法
 * @Author 叶秋 
 * @Date 2021/11/10 22:24
 * @param 
 * @return 
 **/
public interface AppealBusiness {


    /**
     * 根据诉求id查询对应的标签
     *
     * @param appealIds 诉求id
     * @return java.util.Map<java.lang.Integer, java.util.List < fun.huixi.weiju.pojo.entity.WjAppealTag>>
     * @Author 叶秋
     * @Date 2021/11/15 23:11
     **/
    Map<Integer, List<WjAppealTag>> queryAppealTagByAppealId(List<Integer> appealIds);


    /**
     *  分页查询诉求
     * @Author 叶秋
     * @Date 2021/11/15 22:11
     * @param userId 查询人id
     * @param pageQueryAppealDTO 查询参数
     * @return fun.huixi.weiju.page.PageData<fun.huixi.weiju.pojo.vo.appeal.AppealItemVO>
     **/
    PageData<AppealItemVO> pageQueryAppeal(Integer userId, PageQueryAppealDTO pageQueryAppealDTO);



    /**
     *  诉求点赞
     * @Author 叶秋
     * @Date 2021/11/16 11:26
     * @param userId 用户id
     * @param appealId 诉求id
     * @return void
     **/
    void endorseAppeal(Integer userId, Integer appealId);


    /**
     *  取消诉求点赞
     * @Author 叶秋
     * @Date 2021/11/16 11:26
     * @param userId 用户id
     * @param appealId 诉求id
     * @return void
     **/
    void cancelEndorseAppeal(Integer userId, Integer appealId);


    /**
     *  保存帖子评论
     * @Author 叶秋
     * @Date 2021/11/16 11:38
     * @param userId 用户id
     * @param saveAppealCommentDTO 保存参数
     * @return void
     **/
    void saveAppealComment(Integer userId, SaveAppealCommentDTO saveAppealCommentDTO);



    /**
     *  分页查询诉求的评论
     * @Author 叶秋
     * @Date 2021/11/16 11:55
     * @param userId 用户id
     * @param pageQueryAppealCommentDTO 分页查询参数
     * @return fun.huixi.weiju.page.PageData<fun.huixi.weiju.pojo.vo.appeal.AppealCommentItemVO>
     **/
    PageData<AppealCommentItemVO> pageQueryAppealComment(Integer userId, PageQueryAppealCommentDTO pageQueryAppealCommentDTO);
}
