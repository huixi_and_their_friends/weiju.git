package fun.huixi.weiju.pojo.vo.appeal;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import fun.huixi.weiju.pojo.entity.WjAppealTag;
import lombok.Data;

import java.time.LocalDateTime;
import java.util.List;

/**
 *  分页查询后的结果
 * @Author 叶秋
 * @Date 2021/11/15 22:08
 * @param
 * @return
 **/
@Data
public class AppealItemVO {


    // 用户信息

    /**
     * 用户id
     */
    private Integer userId;

    /**
     * 昵称
     */
    private String nickName;

    /**
     * 头像对应的URL地址
     */
    private String headPortrait;



    // 诉求信息


    /**
     * 诉求的id
     */
    private Integer appealId;


    /**
     * 诉求的标题
     */
    private String title;

    /**
     * 诉求的内容
     */
    private String content;


    /**
     * 诉求点赞量
     */
    private Integer endorseCount;


    /**
     *  查询的用户是否点赞
     **/
    private Boolean orEndorse;



    /**
     * 诉求的评论量
     */
    private Integer commentCount;

    /**
     * 诉求浏览量
     */
    private Integer browseCount;


    /**
     * 发诉发布时间
     */
    private LocalDateTime createTime;


    // 诉求附加信息

    /**
     * 发诉标签
     */
    private List<WjAppealTag> tagList;




}
