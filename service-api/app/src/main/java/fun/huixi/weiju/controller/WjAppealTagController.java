package fun.huixi.weiju.controller;


import fun.huixi.weiju.business.AppealBusiness;
import fun.huixi.weiju.pojo.entity.WjAppealTag;
import fun.huixi.weiju.service.WjAppealTagService;
import fun.huixi.weiju.util.wrapper.ResultData;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;
import fun.huixi.weiju.base.BaseController;

import javax.annotation.Resource;
import java.util.List;

/**
 * 诉求-对应标签
 *
 * @author 叶秋
 * @since 2021-11-02
 */
@RestController
@RequestMapping("/wjAppealTag")
public class WjAppealTagController extends BaseController {


    @Resource
    private WjAppealTagService appealTagService;

    
    /**
     *  查询所有的帖子标签
     * @Author 叶秋 
     * @Date 2021/11/10 22:31
     * @return fun.huixi.weiju.util.wrapper.ResultData
     **/
    @PostMapping("queryAllAppealTag")
    public ResultData<List<WjAppealTag>> queryAllAppealTag(){

        List<WjAppealTag> wjAppealTags = appealTagService.list();

        return ResultData.ok(wjAppealTags);
    }


}

