package fun.huixi.weiju.util;

import com.alibaba.fastjson.JSON;
import fun.huixi.weiju.config.JwtConfig;
import fun.huixi.weiju.pojo.vo.user.UserTokenVO;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import lombok.extern.slf4j.Slf4j;

import java.util.Date;

/**
 * JWT工具类
 * @Author Sans
 * @CreateTime 2019/10/2 7:42
 */
@Slf4j
public class JwtTokenUtil {


    /**
     *  生成token
     * @Author 叶秋 
     * @Date 2021/9/14 10:49
     * @param
     * @return java.lang.String
     **/
    public static String createAccessToken(UserTokenVO userTokenVO){
        // 登陆成功生成JWT
        String token = Jwts.builder()
                // 放入用户名和用户ID
                .setId(userTokenVO.getUserId() + "")
                // 主题
                .setSubject(userTokenVO.getNickName())
                // 签发时间
                .setIssuedAt(new Date())
                // 签发者
                .setIssuer("huixi")
                // 自定义属性 放入用户拥有权限
                .claim("authorities", JSON.toJSONString(userTokenVO.getAuthorities()))
                .claim("user", JSON.toJSONString(userTokenVO))
                // 失效时间
                .setExpiration(new Date(System.currentTimeMillis() + JwtConfig.expiration))
                // 签名算法和密钥
                .signWith(SignatureAlgorithm.HS512, JwtConfig.secret)
                .compact();
        return token;
    }



}
